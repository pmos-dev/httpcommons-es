import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsInternalHttpClientImplementationService } from './services/commons-internal-http-client-implementation.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: []
})
export class NgxHttpCommonsEsHttpModule {
	static forRoot(): ModuleWithProviders<NgxHttpCommonsEsHttpModule> {
		return {
				ngModule: NgxHttpCommonsEsHttpModule,
				providers: [
						CommonsInternalHttpClientImplementationService
				]
		};
	}
}
