import { EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { ICommonsNetworkPacket } from 'tscommons-es-network';

import { CommonsNetworkService } from '../services/commons-network.service';

import { ICommonsRemoteControlCommand, isICommonsRemoteControlCommand } from '../interfaces/icommons-remote-control-command';

export class CommonsRemoteControl {
	public onRemoteControlCommands: Map<string, EventEmitter<any|undefined>> = new Map<string, EventEmitter<any|undefined>>();
	
	constructor(
			private networkService: CommonsNetworkService,
			private ns: string
	) {
		this.networkService.receiveObservable.subscribe(
				(packet: ICommonsNetworkPacket): void => {
					if (packet.ns !== ns || packet.command !== 'remote-control') return;
					
					const remoteControlCommand: unknown = packet.data;
					if (!isICommonsRemoteControlCommand(remoteControlCommand)) {
						console.error('Invalid remote control packet received');
						return;
					}
					
					if (!this.onRemoteControlCommands.has(remoteControlCommand.command)) return;
					this.onRemoteControlCommands.get(remoteControlCommand.command)!.emit(remoteControlCommand.data);
				}
		);
	}

	public remoteControlCommandObservable(command: string): Observable<any|undefined> {
		if (!this.onRemoteControlCommands.has(command)) this.onRemoteControlCommands.set(command, new EventEmitter<any|undefined>(true));
		
		return this.onRemoteControlCommands.get(command)!;
	}

	public async sendCommand(command: string, data?: any): Promise<void> {
		const c: ICommonsRemoteControlCommand = {
				command: command,
				data: data
		};
		
		await this.networkService.broadcast(
				this.ns,
				'remote-control',
				c,
				CommonsNetworkService.expiryDate(60),
				false
		);
	}
}
