import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export interface ICommonsRemoteControlCommand {
		command: string;
		data?: any;
}
export function isICommonsRemoteControlCommand(test: any): test is ICommonsRemoteControlCommand {
	if (!commonsTypeHasPropertyString(test, 'command')) return false;
	
	return true;
}
