import { EventEmitter } from '@angular/core';

import { ICommonsNetworkHandshake } from 'tscommons-es-network';

import { ECommonsSignalStrength } from '../enums/ecommons-signal-strength';

import { CommonsNetworkMultiService } from './commons-network-multi.service';
import { CommonsNetworkSocketIoService } from './commons-network-socket-io.service';

export class CommonsNetworkMultiSocketIoService extends CommonsNetworkMultiService {
	private onAnyConnected: EventEmitter<void> = new EventEmitter<void>(true);
	private onNoneConnected: EventEmitter<void> = new EventEmitter<void>(true);
	private onPeerNumberChanged: EventEmitter<number> = new EventEmitter<number>(true);
	private onSignalStrengthChanged: EventEmitter<ECommonsSignalStrength> = new EventEmitter<ECommonsSignalStrength>(true);
	private onPeerConnected: EventEmitter<string> = new EventEmitter<string>(true);
	private onPeerDisconnected: EventEmitter<string> = new EventEmitter<string>(true);
	
	private connected: string[] = [];
	
	constructor(
			private urls: string[],
			compress: boolean
	) {
		super();
		
		for (const url of this.urls) {
			const peer: CommonsNetworkSocketIoService = new CommonsNetworkSocketIoService(url, compress);
			super.addPeer(peer);
			
			peer.connectObservable.subscribe((): void => {
				this.connected.push(url);

				if (this.connected.length === 1) this.onAnyConnected.emit();
				this.onPeerNumberChanged.emit(this.connected.length);
				this.onPeerConnected.emit(url);
				
				switch (this.connected.length) {
					case 0:	// just to catch it in case
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.DISCONNECTED);
						break;
					case 1:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.LOW);
						break;
					case 2:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.MEDIUM);
						break;
					default:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.HIGH);
						break;
				}
			});
			
			peer.disconnectObservable.subscribe((): void => {
				if (this.connected.length === 0) {
					console.log('CommonsNetworkMultiSocketIo connection count has tried to go negative. This should not be possible?');
					return;
				}
				
				this.connected = this.connected
						.filter((u: string): boolean => u !== url);
				
				if (this.connected.length === 0) this.onNoneConnected.emit();
				this.onPeerNumberChanged.emit(this.connected.length);
				this.onPeerDisconnected.emit(url);

				switch (this.connected.length) {
					case 0:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.DISCONNECTED);
						break;
					case 1:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.LOW);
						break;
					case 2:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.MEDIUM);
						break;
					default:
						this.onSignalStrengthChanged.emit(ECommonsSignalStrength.HIGH);
						break;
				}
			});
		}
	}
	
	public get peersUrl(): string[] {
		return [ ...this.urls ];
	}
	
	public get connectedPeersUrl(): string[] {
		return [ ...this.connected ];
	}

	public get connectObservable(): EventEmitter<void> {
		return this.onAnyConnected;
	}

	public get disconnectObservable(): EventEmitter<void> {
		return this.onNoneConnected;
	}

	public get peerNumberChangedObservable(): EventEmitter<number> {
		return this.onPeerNumberChanged;
	}

	public get signalStrengthChangedObservable(): EventEmitter<ECommonsSignalStrength> {
		return this.onSignalStrengthChanged;
	}

	public get peerConnectedObservable(): EventEmitter<string> {
		return this.onPeerConnected;
	}

	public get peerDisconnectedObservable(): EventEmitter<string> {
		return this.onPeerDisconnected;
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		let anySuccess: boolean = false;
		
		for (const peer of this.peers) anySuccess = (peer as CommonsNetworkSocketIoService).connect(params) || anySuccess;
		
		return anySuccess;
	}
	
	// sometimes various ngInits happen after the initial connect
	public triggerPeerNumberChanged(): void {
		this.onPeerNumberChanged.emit(this.connected.length);
			
		switch (this.connected.length) {
			case 0:
				this.onSignalStrengthChanged.emit(ECommonsSignalStrength.DISCONNECTED);
				break;
			case 1:
				this.onSignalStrengthChanged.emit(ECommonsSignalStrength.LOW);
				break;
			case 2:
				this.onSignalStrengthChanged.emit(ECommonsSignalStrength.MEDIUM);
				break;
			default:
				this.onSignalStrengthChanged.emit(ECommonsSignalStrength.HIGH);
				break;
		}
	}

	public async requestReplay(): Promise<void> {
		for (const peer of this.peers) {
			(peer as CommonsNetworkSocketIoService).requestReplay();
		}
	}
}
