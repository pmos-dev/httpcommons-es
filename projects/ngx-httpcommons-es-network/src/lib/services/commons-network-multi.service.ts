import { ICommonsNetworkPacket } from 'tscommons-es-network';

import { CommonsNetworkService } from './commons-network.service';

export class CommonsNetworkMultiService extends CommonsNetworkService {
	protected peers: CommonsNetworkService[] = [];
	
	public addPeer(peer: CommonsNetworkService): void {
		this.peers.push(peer);
		
		peer.receiveObservable
				.subscribe(
						(packet: ICommonsNetworkPacket): void => {
							this.incoming(packet);
						}
				);
	}

	public async transmit(packet: ICommonsNetworkPacket): Promise<any> {
		const promises: Promise<any>[] = [];
		for (const peer of this.peers) {
			promises.push(peer.transmit(packet));
		}
		
		await Promise.all(promises);
	}
	
	public override get channel(): string|undefined {
		return super.channel;
	}
	public override set channel(channel: string|undefined) {
		super.channel = channel;
		for (const peer of this.peers) peer.channel = channel;
	}
	
	public override get deviceId(): string|undefined {
		return super.deviceId;
	}
	public override set deviceId(id: string|undefined) {
		super.deviceId = id;
		for (const peer of this.peers) peer.deviceId = id;
	}
}
