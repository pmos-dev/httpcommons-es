import { Observable } from 'rxjs';

import { commonsTypeDecodePropertyObject, commonsTypeEncodePropertyObject, commonsTypeIsEncodedObject, commonsTypeIsString } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		TCommonsNetworkPacketCompressed,
		isTCommonsNetworkPacketCompressed,
		compressPacket,
		decompressPacket
} from 'tscommons-es-network';
import { ICommonsNetworkHandshake } from 'tscommons-es-network';

import { commonsSocketIoBuildOptions, commonsSocketIoBuildUrl, CommonsSocketIoClientService } from 'ngx-httpcommons-es-socket-io';

import { CommonsNetworkService } from './commons-network.service';

class SocketIoClientService extends CommonsSocketIoClientService {
	constructor(
			url: string,
			private callback: (data: unknown) => void
	) {
		super(
				commonsSocketIoBuildUrl(url),
				true,
				commonsSocketIoBuildOptions(url)
		);
	}

	protected setupOns(): void {
		this.on('rx', (data: unknown): void => {
			this.callback(data);
		});
	}
	
	public async emit(command: string, data: any): Promise<unknown> {
		return await super.emit(command, data);
	}
}

export class CommonsNetworkSocketIoService extends CommonsNetworkService {
	socketIoClientService: SocketIoClientService;
	
	constructor(
			url: string,
			private compress: boolean = false
	) {
		super();
		
		this.socketIoClientService = new SocketIoClientService(
				url,
				(data: unknown): void => {
					if (!commonsTypeIsEncodedObject(data)) {
						console.log('Invalid encoded packet');
						return;
					}
					
					const decoded: TPropertyObject = commonsTypeDecodePropertyObject(data);

					let packet: ICommonsNetworkPacket;
					
					if (this.compress) {
						if (!isTCommonsNetworkPacketCompressed(decoded)) {
							console.log('Invalid compressed packet');
							return;
						}
						
						packet = decompressPacket(decoded);
					} else {
						if (!isICommonsNetworkPacket(decoded)) {
							console.log('Invalid packet');
							return;
						}
						
						packet = decoded;
					}
					
					this.incoming(packet);
				}
		);
	}

	public get connectObservable(): Observable<void> {
		return this.socketIoClientService.connectObservable;
	}

	public get disconnectObservable(): Observable<void> {
		return this.socketIoClientService.disconnectObservable;
	}

	// "errored" is used to avoid a name clash with the Ticketed interface already using "errorObservable"
	public get erroredObservable(): Observable<Error> {
		return this.socketIoClientService.erroredObservable;
	}
	
	public async transmit(packet: ICommonsNetworkPacket): Promise<unknown> {
		let encoded: TEncodedObject;
		
		if (this.compress) {
			const compressed: TCommonsNetworkPacketCompressed = compressPacket(packet);
			encoded = commonsTypeEncodePropertyObject(compressed);
		} else encoded = commonsTypeEncodePropertyObject(packet);
		
		return await this.socketIoClientService.emit('tx', encoded);
	}
	
	public async requestReplay(): Promise<void> {
		const since: string|undefined = this.lastSeen;
		if (since === undefined) return;
		
		try {
			const lastSeen: unknown = await this.socketIoClientService.emit('replay', {
					since: since,
					channel: this.channel,
					deviceId: this.deviceId
			});
			if (commonsTypeIsString(lastSeen)) {
				this.lastSeen = lastSeen;
			}
		} catch (e) {
			console.log('Replay request failed');
		}
		return;
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		if (this.deviceId === undefined) throw new Error('No deviceId set prior to connecting');
		if (this.channel === undefined) throw new Error('No channel set prior to connecting');

		if (params === undefined) {
			params = {
					channel: this.channel,
					deviceId: this.deviceId
			};
		} else {
			params.channel = this.channel;
			params.deviceId = this.deviceId;
		}
				
		return this.socketIoClientService.connect(params as any);
	}
}
