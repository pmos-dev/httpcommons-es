import { EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';
import { ICommonsNetworkPacket, ETransmissionMethod } from 'tscommons-es-network';

export abstract class CommonsNetworkService {
	public static generateDeviceId(): string {
		return commonsBase62GenerateRandomId();
	}
	
	public static generatePacketId(): string {
		return commonsBase62GenerateRandomId();
	}
	
	public static expiryDate(seconds: number): Date {
		const date: Date = new Date();
		date.setTime(date.getTime() + (seconds * 1000));
		
		return date;
	}
	
	private onReceive = new EventEmitter<ICommonsNetworkPacket>(true);

	private internalChannel?: string;
	public get channel(): string|undefined {
		return this.internalChannel;
	}
	public set channel(channel: string|undefined) {
		this.internalChannel = channel;
	}

	private internalDeviceId?: string;
	public get deviceId(): string|undefined {
		return this.internalDeviceId;
	}
	public set deviceId(id: string|undefined) {
		this.internalDeviceId = id;
	}

	private internalLastSeen: string|undefined;
	public get lastSeen(): string|undefined {
		return this.internalLastSeen;
	}
	public set lastSeen(lastSeen: string|undefined) {
		this.internalLastSeen = lastSeen;
	}

	private seen: string[] = [];
	
	public get receiveObservable(): Observable<ICommonsNetworkPacket> {
		return this.onReceive;
	}

	protected incoming(packet: ICommonsNetworkPacket): void {
		if (packet.channel !== this.internalChannel) return;
		if (packet.expiry && packet.expiry.getTime() < new Date().getTime()) return;

		if (this.seen.includes(packet.id)) return;
		this.seen.push(packet.id);
		
		if (!packet.replay) this.internalLastSeen = packet.id;
	
		if (
			(packet.method === ETransmissionMethod.BROADCAST && (!packet.ignoreOwn || (packet.source !== this.internalDeviceId)))
			|| ((packet.method === ETransmissionMethod.DIRECT || packet.method === ETransmissionMethod.MULTICAST) && packet.destination === this.internalDeviceId)
		) {
			this.onReceive.emit(packet);
		}
	}
	
	public abstract transmit(packet: ICommonsNetworkPacket): Promise<any>;
	
	public isSelf(id: string): boolean {
		if (this.internalDeviceId === undefined) return false;
		return this.internalDeviceId === id;
	}
	
	public async broadcast(ns: string, command: string, data?: any, expiry?: Date, ignoreOwn?: boolean): Promise<void> {
		if (this.internalDeviceId === undefined) throw new Error('No deviceId set prior to broadcasting');
		if (this.internalChannel === undefined) throw new Error('No channel set prior to broadcasting');
		
		const packet: ICommonsNetworkPacket = {
				id: CommonsNetworkService.generatePacketId(),
				method: ETransmissionMethod.BROADCAST,
				channel: this.internalChannel,
				source: this.internalDeviceId,
				timestamp: new Date(),
				ns: ns,
				command: command
		};
		if (data !== undefined) packet.data = data;
		if (ignoreOwn) packet.ignoreOwn = true;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
	
	public async direct(destination: string, ns: string, command: string, data?: any, expiry?: Date): Promise<void> {
		if (this.internalDeviceId === undefined) throw new Error('No deviceId set prior to direct');
		if (this.internalChannel === undefined) throw new Error('No channel set prior to direct');
		
		const packet: ICommonsNetworkPacket = {
				id: CommonsNetworkService.generatePacketId(),
				method: ETransmissionMethod.DIRECT,
				channel: this.internalChannel,
				destination: destination,
				source: this.internalDeviceId,
				timestamp: new Date(),
				ns: ns,
				command: command
		};
		if (data !== undefined) packet.data = data;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
	
	public async multicast(destinations: string[], ns: string, command: string, data?: any, expiry?: Date): Promise<void> {
		if (this.internalDeviceId === undefined) throw new Error('No deviceId set prior to multicast');
		if (this.internalChannel === undefined) throw new Error('No channel set prior to multicast');
		
		const packet: ICommonsNetworkPacket = {
				id: CommonsNetworkService.generatePacketId(),
				method: ETransmissionMethod.MULTICAST,
				channel: this.internalChannel,
				destinations: destinations,
				source: this.internalDeviceId,
				timestamp: new Date(),
				ns: ns,
				command: command
		};
		if (data !== undefined) packet.data = data;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
}
