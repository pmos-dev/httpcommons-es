/*
 * Public API Surface of ngx-httpcommons-es-socket-io-pseudo-http
 */

export * from './lib/services/commons-internal-socket-io-pseudo-http-client-implementation.service';

export * from './lib/ngx-httpcommons-es-socket-io-pseudo-http.module';
