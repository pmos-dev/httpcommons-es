import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxHttpCommonsEsSocketIoModule } from 'ngx-httpcommons-es-socket-io';

@NgModule({
		imports: [
				CommonModule,
				NgxHttpCommonsEsSocketIoModule
		],
		declarations: [],
		exports: []
})
export class NgxHttpCommonsEsSocketIoTicketedModule {}
