/*
 * Public API Surface of ngx-httpcommons-es-socket-io-ticketed
 */

export * from './lib/services/commons-ticketed-generic-socket-io-client.service';

export * from './lib/ngx-httpcommons-es-socket-io-ticketed.module';
