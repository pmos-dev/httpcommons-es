import { ManagerOptions } from 'socket.io-client';

export function commonsSocketIoBuildOptions(url: string): Partial<ManagerOptions> {
	const options: Partial<ManagerOptions> = {
			transports: [ 'websocket', 'polling' ]
	};

	if (url.startsWith('/')) {
		options.path = `${url}${url.endsWith('/') ? '' : '/'}socket.io`;
	} else {
		const regexp: RegExp = /^http(?:s?):\/\/[^\/]+\/(.+)$/i;
		const match: RegExpExecArray|null = regexp.exec(url);
		
		if (match !== null) {
			options.path = `/${match[1]}${match[1].endsWith('/') ? '' : '/'}socket.io`;
		}
	}
	
	return options;
}

export function commonsSocketIoBuildUrl(url: string): string {
	if (url.startsWith('/')) return '/';
	
	const regexp: RegExp = /^(http(?:s?):\/\/[^\/]+\/).+$/i;
	const match: RegExpExecArray|null = regexp.exec(url);
	
	if (match !== null) return match[1];
	
	return url;
}
