import { ManagerOptions } from 'socket.io-client';

import { CommonsSocketIoClientService } from './commons-socket-io-client.service';

type TConnectionCallback = () => void;
type TErrorCallback = (error: Error) => void;

// This is a helper service that mimicks the "add*Callback" methods approach that equivalent CommonsSocketIoClientService in Node uses.
// Used to help match interface requirements in cross-platform etc.

export abstract class CommonsSocketIoClientUsingCallbacksService extends CommonsSocketIoClientService {
	private connectCallbacks: TConnectionCallback[] = [];
	private disconnectCallbacks: TConnectionCallback[] = [];
	private errorCallbacks: TErrorCallback[] = [];

	constructor(
			url: string,
			defaultEnabled?: boolean|undefined,
			options?: Partial<ManagerOptions>
	) {
		super(
				url,
				defaultEnabled,
				options
		);

		this.connectObservable
				.subscribe((): void => {
					for (const callback of this.connectCallbacks) callback();
				});
		this.disconnectObservable
				.subscribe((): void => {
					for (const callback of this.disconnectCallbacks) callback();
				});
		this.erroredObservable
				.subscribe((e: Error): void => {
					for (const callback of this.errorCallbacks) callback(e);
				});
	}

	public addConnectCallback(callback: TConnectionCallback): void {
		this.connectCallbacks.push(callback);
	}

	public addDisconnectCallback(callback: TConnectionCallback): void {
		this.disconnectCallbacks.push(callback);
	}

	public addErrorCallback(callback: TErrorCallback): void {
		this.errorCallbacks.push(callback);
	}
}
