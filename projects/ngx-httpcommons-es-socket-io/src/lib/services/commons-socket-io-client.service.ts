import { EventEmitter } from '@angular/core';

import { Subscription } from 'rxjs';
import { io, Socket, ManagerOptions } from 'socket.io-client';

import { commonsBase62IsId, commonsTypeIsString } from 'tscommons-es-core';

export abstract class CommonsSocketIoClientService {
	private connectEmitter: EventEmitter<void> = new EventEmitter<void>(true);
	private disconnectEmitter: EventEmitter<void> = new EventEmitter<void>(true);
	// errored is a poor naming, but errorObservable is already used in the Ticketed interface for something else, and since this Emitter has been introducted afterwards, that has priority
	private erroredEmitter: EventEmitter<Error> = new EventEmitter<Error>(true);

	private socket?: Socket;
	private connected: boolean = false;
	private enabled: boolean|undefined;

	constructor(
			private url: string,
			private defaultEnabled?: boolean|undefined,
			private options?: Partial<ManagerOptions>
	) {}

	public get connectObservable(): EventEmitter<void> {
		return this.connectEmitter;
	}

	public get disconnectObservable(): EventEmitter<void> {
		return this.disconnectEmitter;
	}

	// See note above about "errored"
	public get erroredObservable(): EventEmitter<Error> {
		return this.erroredEmitter;
	}

	// This isn't part of the socket.io library, so requests any server to implement it manually
	public async getId(): Promise<string|undefined> {
		const result: unknown = await this.emit('commons-socket.io-identify-self-uid', {});
		if (!commonsTypeIsString(result)) return undefined;
		if (!commonsBase62IsId(result)) return undefined;

		return result;
	}

	public connect(params?: { [ key: string ]: string|number|boolean }|undefined): boolean {
		if (this.socket) return true;	// already connected

		const options: Partial<ManagerOptions> = this.options || {};
		
		if (params) {
			const asStrings: { [ key: string ]: string } = {};
			
			for (const key of Object.keys(params)) {
				asStrings[key] = params[key].toString();
			}
			options.query = asStrings;
		}

		this.socket = io(this.url, options);
		if (!this.socket) return false;
		
		this.socket.on('connect', (): void => {
			this.connected = true;
			if (this.enabled === undefined) this.enabled = this.defaultEnabled === false ? false : true;
			this.connectEmitter.emit();
		});
		this.socket.on('disconnect', (): void => {
			this.connected = false;
			this.disconnectEmitter.emit();
		});
		this.socket.on('error', (e: Error): void => {
			this.erroredEmitter.emit(e);
		});
		this.socket.on('connect_error', (e: Error): void => {
			this.erroredEmitter.emit(e);
		});

		this.setupOns();
		
		return true;
	}
	
	public connectAndAwait(timeout: number, params?: { [ key: string ]: string|number|boolean }|undefined): Promise<boolean> {
		return new Promise((resolve: (_: boolean) => void, _): void => {
			if (!this.connect(params)) {
				resolve(false);
				return;
			}
			
			if (this.connected) {
				resolve(true);
				return;
			}
			
			let subscriptionConnect: Subscription|undefined;
			let subscriptionDisconnect: Subscription|undefined;
			let subscriptionTimeout: any|undefined;
			
			subscriptionConnect = this.connectEmitter.subscribe(
					(): void => {
						if (subscriptionTimeout) {
							clearTimeout(subscriptionTimeout);
							subscriptionTimeout = undefined;
						}
						if (subscriptionConnect) {
							subscriptionConnect.unsubscribe();
							subscriptionConnect = undefined;
						}
						if (subscriptionDisconnect) {
							subscriptionDisconnect.unsubscribe();
							subscriptionDisconnect = undefined;
						}
						resolve(true);
					}
			);
			
			subscriptionDisconnect = this.disconnectEmitter.subscribe(
					(): void => {
						if (subscriptionTimeout) {
							clearTimeout(subscriptionTimeout);
							subscriptionTimeout = undefined;
						}
						if (subscriptionConnect) {
							subscriptionConnect.unsubscribe();
							subscriptionConnect = undefined;
						}
						if (subscriptionDisconnect) {
							subscriptionDisconnect.unsubscribe();
							subscriptionDisconnect = undefined;
						}
						resolve(false);
					}
			);
			
			subscriptionTimeout = setTimeout((): void => {
				if (subscriptionTimeout) {
					clearTimeout(subscriptionTimeout);
					subscriptionTimeout = undefined;
				}
				if (subscriptionConnect) {
					subscriptionConnect.unsubscribe();
					subscriptionConnect = undefined;
				}
				if (subscriptionDisconnect) {
					subscriptionDisconnect.unsubscribe();
					subscriptionDisconnect = undefined;
				}
				resolve(false);
			}, timeout);
		});
	}

	public disconnect(): void {
		if (this.socket === undefined) return;
		this.socket.disconnect();
	}

	protected abstract setupOns(): void;

	public setEnabled(state: boolean): void {
		this.enabled = state;
	}
	
	public getEnabled(): boolean {
		return this.enabled ? true : false;
	}

	public isConnected(): boolean {
		return this.socket !== undefined && this.connected;
	}

	protected on(
			command: string,
			callback: (data: unknown) => void
	): void {
		if (!this.socket) return;	// shouldn't really be possible if the class is being used properly with setupOns
		
		this.socket.on(
				command,
				(data: unknown): void => {
					if (!this.isConnected()) return;	// is this possible?
					if (!this.getEnabled()) return;
					
					callback(data);
				}
		);
	}

	protected async emit(
			command: string,
			data: any
	): Promise<unknown> {
		return new Promise((resolve: (_: unknown) => void, reject): void => {
			if (this.socket === undefined) {
				reject('No socket yet');
				return;
			}
		
			if (!this.isConnected()) {
				reject('Not connected');
				return;
			}
		
			if (!this.getEnabled()) {
				reject('Not enabled');
				return;
			}
		
			this.socket.emit(command, data, (response: unknown): void => {
				resolve(response);
			});
		});
	}
}
