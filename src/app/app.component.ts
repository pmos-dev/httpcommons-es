import { Component, OnInit } from '@angular/core';

import { ECommonsHttpResponseDataType } from 'tscommons-es-http';

import { CommonsInternalHttpClientImplementationService } from 'ngx-httpcommons-es-http';

@Component({
		selector: 'app-root',
		templateUrl: './app.component.html',
		styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
	title = 'HttpcommonsEs';

	constructor(
			private http: CommonsInternalHttpClientImplementationService
	) {}

	async ngOnInit(): Promise<void> {
		const result: unknown = await this.http.internalGet(
				'http://localhost:4200/',
				{},
				{},
				ECommonsHttpResponseDataType.STRING,
				{}
		);
		console.log(result);
	}
}
