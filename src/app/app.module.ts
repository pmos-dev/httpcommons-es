import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { NgxHttpCommonsEsHttpModule } from 'ngx-httpcommons-es-http';

import { AppComponent } from './app.component';

@NgModule({
		declarations: [
				AppComponent
		],
		imports: [
				BrowserModule,
				HttpClientModule,
				NgxHttpCommonsEsHttpModule.forRoot()
		],
		providers: [],
		bootstrap: [AppComponent]
})
export class AppModule { }
